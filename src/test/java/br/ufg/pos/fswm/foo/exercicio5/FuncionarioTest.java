package br.ufg.pos.fswm.foo.exercicio5;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class FuncionarioTest {

    public static final double DELTA = 0.0001;
    private Funcionario sut;

    @Before
    public void setUp() throws Exception {
        sut = new Funcionario();
    }

    @Test
    public void deve_ser_possivel_informar_o_salario_fixo_do_funcionario() throws Exception {
        sut.setSalarioFixo(500.0);

        assertEquals(500.0, sut.getSalarioFixo(), DELTA);
    }

    @Test
    public void deve_ser_possivel_informar_o_numero_de_inscricao_do_funcionario() throws Exception {
        sut.setNumeroInscricao(1234);

        assertEquals(1234, sut.getNumeroInscricao());
    }

    @Test
    public void deve_ser_possivel_informar_a_quantidade_de_televisores_de_tela_plana_vendida_pelo_funcionario() throws Exception {
        sut.setQtdTvTelaPlanaVendida(25);

        assertEquals(25, sut.getQtdTvTelaPlanaVendida());
    }

    @Test
    public void deve_ser_possivel_informar_a_quantidade_de_televisores_de_tela_comum_vendida_pelo_funcionario() throws Exception {
        sut.setQtdTvTelaComumVendida(30);

        assertEquals(30, sut.getQtdTvTelaComumVendida());
    }

    @Test
    public void deve_calcular_comissao_de_funcionario_que_vendeu_menos_que_10_tv_de_tela_plana() throws Exception {
        sut.setQtdTvTelaPlanaVendida(9);

        assertEquals(270, sut.calcularComissao(), DELTA);
    }

    @Test
    public void deve_calcular_comissao_de_funcionario_que_vendeu_mais_que_10_tv_de_tela_plana() throws Exception {
        sut.setQtdTvTelaPlanaVendida(15);

        assertEquals(750.0, sut.calcularComissao(), DELTA);
    }

    @Test
    public void deve_calcular_comissao_de_funcionario_que_vendeu_menos_que_20_tv_de_tela_comum() throws Exception {
        sut.setQtdTvTelaComumVendida(15);

        assertEquals(300.0, sut.calcularComissao(), DELTA);
    }

    @Test
    public void deve_calcular_comissao_de_funcionario_que_vendeu_mais_que_20_tv_de_tela_comum() throws Exception {
        sut.setQtdTvTelaComumVendida(25);

        assertEquals(750.0, sut.calcularComissao(), DELTA);
    }

    @Test
    public void deve_ser_possivel_calcular_o_INSS_de_um_funcionario() throws Exception {
        sut.setSalarioFixo(500.0);
        sut.setQtdTvTelaComumVendida(10);
        sut.setQtdTvTelaPlanaVendida(10);

        assertEquals(96.0, sut.calcularINSS(), DELTA);
    }

    @Test
    public void deve_ser_possivel_calcular_valor_do_imposto_de_renda_retido_na_fonte() throws Exception {
        sut.setSalarioFixo(500.0);
        sut.setQtdTvTelaComumVendida(10);
        sut.setQtdTvTelaPlanaVendida(10);

        assertEquals(55.2, sut.calcularIR(), DELTA);
    }

    @Test
    public void deve_isentar_funcionario_de_imposto_de_renda_se_seu_salario_for_menor_que_700() throws Exception {
        sut.setSalarioFixo(50);

        assertEquals(0.0, sut.calcularIR(), DELTA);
    }

    @Test
    public void deve_calcular_salario_bruto_de_funcionario() throws Exception {
        sut.setSalarioFixo(500.0);
        sut.setQtdTvTelaComumVendida(10);
        sut.setQtdTvTelaPlanaVendida(10);

        assertEquals(1200.0, sut.calcularSalarioBruto(), DELTA);
    }

    @Test
    public void deve_calcular_salario_liquido_de_funcionario() throws Exception {
        sut.setSalarioFixo(500.0);
        sut.setQtdTvTelaComumVendida(10);
        sut.setQtdTvTelaPlanaVendida(10);

        assertEquals(1048.8, sut.calcularSalarioLiquido(), DELTA);
    }
}
