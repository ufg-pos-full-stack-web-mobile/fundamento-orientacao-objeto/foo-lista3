package br.ufg.pos.fswm.foo.exercicio2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class RacionalTest {

    private Racional sut;

    @Test
    public void deve_ser_possivel_informar_um_numerador_inteiro() throws Exception {
        sut = new Racional();

        sut.setNumerador(1);

        assertEquals(1, sut.getNumerador());
    }

    @Test
    public void deve_ser_possivel_informar_um_denominador_inteiro() throws Exception {
        sut = new Racional();

        sut.setDenominador(2);

        assertEquals(2, sut.getDenominador());
    }

    @Test
    public void deve_ser_possivel_iniciar_numerador_e_denominador_na_instanciacao_da_classe() throws Exception {
        sut = new Racional(1, 2);

        assertEquals(1, sut.getNumerador());
        assertEquals(2, sut.getDenominador());
    }

    @Test
    public void deve_armazenar_valor_reduzido_da_fracao_ao_inicializar_racional() throws Exception {
        sut = new Racional(30, 48);

        assertEquals(5, sut.getNumerador());
        assertEquals(8, sut.getDenominador());
    }

    @Test
    public void deve_armazenar_valor_reduzido_da_fracao_ao_inicializar_racional_teste_2() throws Exception {
        sut = new Racional(50, 250);

        assertEquals(1, sut.getNumerador());
        assertEquals(5, sut.getDenominador());
    }

    @Test
    public void deve_ser_possivel_somar_dois_racionais() throws Exception {
        sut = new Racional(50, 250);

        sut.somar(new Racional(80, 120));

        assertEquals(13, sut.getNumerador());
        assertEquals(15, sut.getDenominador());
    }

    @Test
    public void deve_ser_possivel_subtrair_dois_racionais() throws Exception {
        sut = new Racional(50, 250);

        sut.subtrair(new Racional(80, 120));

        assertEquals(-7, sut.getNumerador());
        assertEquals(15, sut.getDenominador());
    }

    @Test
    public void deve_ser_possivel_multiplicar_dois_racionais() throws Exception {
        sut = new Racional(50, 250);

        sut.multiplicar(new Racional(80, 120));

        assertEquals(2, sut.getNumerador());
        assertEquals(15, sut.getDenominador());
    }

    @Test
    public void deve_ser_possivel_dividir_dois_racionais() throws Exception {
        sut = new Racional(50, 250);

        sut.dividir(new Racional(80, 120));

        assertEquals(3, sut.getNumerador());
        assertEquals(10, sut.getDenominador());
    }

    @Test
    public void deve_mostrar_numero_racional_na_forma_reduzida() throws Exception {
        sut = new Racional(50, 250);

        String resultado = sut.mostrar();

        assertEquals("1/5", resultado);
    }
}
