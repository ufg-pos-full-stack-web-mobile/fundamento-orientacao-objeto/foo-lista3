package br.ufg.pos.fswm.foo.exercicio6;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class OperarioTest {

    public static final double DELTA = 0.0001;
    private Operario sut;

    @Before
    public void setUp() throws Exception {
        sut = new Operario();
    }

    @Test
    public void deve_ser_possivel_informar_a_matricula_do_operario() throws Exception {
        final int matricula = 1234;

        sut.setMatricula(matricula);

        assertEquals(matricula, sut.getMatricula());
    }

    @Test
    public void deve_ser_possivel_informar_o_numero_de_pecas_fabricadas_por_mes() throws Exception {
        final int pecasFabricadas = 50;

        sut.setPecasFabricadas(pecasFabricadas);

        assertEquals(pecasFabricadas, sut.getPecasFabricadas());
    }

    @Test
    public void deve_ser_possivel_informar_o_sexo_do_operario() throws Exception {
        final Sexo sexo = Sexo.MASCULINO;

        sut.setSexo(sexo);

        assertEquals(sexo, sut.getSexo());
    }

    @Test
    public void deve_calcular_o_salario_de_um_funcionario_da_categoria_A() throws Exception {
        final int pecasFabricadas = 10;

        sut.setPecasFabricadas(pecasFabricadas);

        assertEquals(SalarioMinimo.VALOR, sut.calcularSalario(), DELTA);
    }

    @Test
    public void deve_calcular_o_salario_de_um_funcionario_da_categoria_B() throws Exception {
        final int pecasFabricadas = 11;

        sut.setPecasFabricadas(pecasFabricadas);

        assertEquals(1050.0, sut.calcularSalario(), DELTA);
    }

    @Test
    public void deve_calcular_o_salario_de_um_funcioanrio_da_categoria_B_que_fabricou_o_maximo_de_pecas() throws Exception {
        final int pecasFabricadas = 35;

        sut.setPecasFabricadas(pecasFabricadas);

        assertEquals(2250.0, sut.calcularSalario(), DELTA);
    }

    @Test
    public void deve_calcular_o_salario_de_um_funcionario_da_categoria_C() throws Exception {
        final int pecasFabricadas = 36;

        sut.setPecasFabricadas(pecasFabricadas);

        assertEquals(1090.0, sut.calcularSalario(), DELTA);
    }

    @Test
    public void deve_calcular_o_salario_de_um_funcionario_da_categoria_C_que_fabricou_mais_pecas() throws Exception {
        final int pecasFabricadas = 50;

        sut.setPecasFabricadas(pecasFabricadas);

        assertEquals(2350.0, sut.calcularSalario(), DELTA);
    }

    @Test
    public void deve_identificar_operario_da_categoria_A() throws Exception {
        sut.setPecasFabricadas(9);

        Categoria categoria = sut.getCategoria();

        assertEquals(Categoria.A, categoria);
    }

    @Test
    public void deve_identificar_operario_da_categoria_B() throws Exception {
        sut.setPecasFabricadas(19);

        Categoria categoria = sut.getCategoria();

        assertEquals(Categoria.B, categoria);
    }

    @Test
    public void deve_identificar_operario_da_categoria_C() throws Exception {
        sut.setPecasFabricadas(39);

        Categoria categoria = sut.getCategoria();

        assertEquals(Categoria.C, categoria);
    }
}
