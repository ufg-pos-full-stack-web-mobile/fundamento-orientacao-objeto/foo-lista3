package br.ufg.pos.fswm.foo.exercicio7;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class GabaritoTest {

    private Gabarito sut;

    @Before
    public void setUp() throws Exception {
        final Map<Integer, Integer> respostas = new HashMap<Integer, Integer>();
        respostas.put(1, 3);
        respostas.put(2, 2);
        respostas.put(3, 1);
        respostas.put(4, 3);
        respostas.put(5, 2);
        respostas.put(6, 5);
        respostas.put(7, 5);
        respostas.put(8, 1);
        respostas.put(9, 2);
        respostas.put(10, 5);

        sut = new Gabarito(respostas);
    }

    @Test
    public void deve_retornar_a_resposta_correta_da_questao() throws Exception {
        assertEquals(3, sut.getRespostaDaQuestao(1).intValue());
        assertEquals(2, sut.getRespostaDaQuestao(2).intValue());
        assertEquals(1, sut.getRespostaDaQuestao(3).intValue());
        assertEquals(3, sut.getRespostaDaQuestao(4).intValue());
        assertEquals(2, sut.getRespostaDaQuestao(5).intValue());
        assertEquals(5, sut.getRespostaDaQuestao(6).intValue());
        assertEquals(5, sut.getRespostaDaQuestao(7).intValue());
        assertEquals(1, sut.getRespostaDaQuestao(8).intValue());
        assertEquals(2, sut.getRespostaDaQuestao(9).intValue());
        assertEquals(5, sut.getRespostaDaQuestao(10).intValue());
    }
}
