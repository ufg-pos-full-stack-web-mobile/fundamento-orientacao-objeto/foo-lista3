package br.ufg.pos.fswm.foo.exercicio7;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class ProvaTest {

    private Prova sut;

    @Before
    public void setUp() throws Exception {
        sut = new Prova();
    }

    @Test
    public void deve_ser_possivel_informar_o_numero_de_inscricao_do_candidato() throws Exception {
        final int numInscricao = 1234;
        sut.setNumInscricao(numInscricao);

        assertEquals(numInscricao, sut.getNumInscricao());
    }

    @Test
    public void deve_ser_possivel_informar_o_nome_do_candidato() throws Exception {
        final String nome = "Bruno";
        sut.setNome(nome);

        assertEquals(nome, sut.getNome());
    }

    @Test
    public void deve_ser_possivel_informar_o_numero_do_telefone_do_candidato() throws Exception {
        final String telefone = "(xx) xxxx-xxxx";
        sut.setTelefone(telefone);

        assertEquals(telefone, sut.getTelefone());
    }

    @Test
    public void deve_ser_possivel_informar_a_resposta_das_questoes_da_prova() throws Exception {
        final int numQuestao = 1;
        final int resposta = 2;

        sut.responder(numQuestao, resposta);

        assertEquals(resposta, sut.getResposta(numQuestao));
    }

    @Test
    public void deve_calcular_nota_de_aluno_dado_gabarito_de_resposta() throws Exception {
        Gabarito gabarito = montarGabarito();

        montarRespostasProva();

        sut.setGabaritoRespostas(gabarito);

        int nota = sut.calcularNota();

        assertEquals(7, nota);
    }

    @Test
    public void deve_retornar_dados_do_candidato_com_nota_da_prova() throws Exception {
        Gabarito gabarito = montarGabarito();

        sut.setNumInscricao(1234);
        sut.setNome("Bruno");
        sut.setTelefone("(62) 99999-9999");
        montarRespostasProva();
        sut.setGabaritoRespostas(gabarito);

        final String esperada = "Dados Candidato:\nInscrição: 1234\nNome: Bruno\nNota: 7";
        final String retornada = sut.monstrarDados();

        assertEquals(esperada, retornada);

    }

    private void montarRespostasProva() {
        sut.responder(1, 5);
        sut.responder(2, 2);
        sut.responder(3, 1);
        sut.responder(4, 3);
        sut.responder(5, 2);
        sut.responder(6, 4);
        sut.responder(7, 5);
        sut.responder(8, 3);
        sut.responder(9, 2);
        sut.responder(10, 5);
    }

    private Gabarito montarGabarito() {
        final Map<Integer, Integer> respostas = new HashMap<Integer, Integer>();
        respostas.put(1, 3);
        respostas.put(2, 2);
        respostas.put(3, 1);
        respostas.put(4, 3);
        respostas.put(5, 2);
        respostas.put(6, 5);
        respostas.put(7, 5);
        respostas.put(8, 1);
        respostas.put(9, 2);
        respostas.put(10, 5);

        return new Gabarito(respostas);
    }
}
