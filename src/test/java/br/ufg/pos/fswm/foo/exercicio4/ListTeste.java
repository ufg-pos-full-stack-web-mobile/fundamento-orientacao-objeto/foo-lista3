package br.ufg.pos.fswm.foo.exercicio4;

import br.ufg.pos.fswm.foo.exercicio3.Cidade;
import br.ufg.pos.fswm.foo.exercicio3.CidadeBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class ListTeste {

    private List sut;

    @Test
    public void deve_ser_possivel_instanciar_uma_list_passando_a_quantidade_de_cidades_possiveis_de_serem_gerenciadas() throws Exception {
        sut = new List(10);

        assertEquals(10, sut.getCapacidadeList());
    }

    @Test
    public void deve_ser_possivel_inserir_cidades_na_lista() throws Exception {
        sut = new List(10);

        Cidade cidade = new CidadeBuilder().comCodigo(1).construir();
        sut.adicionar(cidade);

        assertEquals(1, sut.cidadesNaLista());
    }

    @Test
    public void deve_ser_possivel_inserir_cidades_em_todas_as_posicoes_da_lista() throws Exception {
        sut = new List(3);

        sut.adicionar(new Cidade());
        sut.adicionar(new Cidade());
        sut.adicionar(new Cidade());

        assertEquals(3, sut.cidadesNaLista());
    }

    @Test
    public void deve_ser_possivel_remover_uma_cidade_da_lista() throws Exception {
        sut = new List(3);

        sut.adicionar(new CidadeBuilder().comCodigo(1).construir());
        sut.adicionar(new CidadeBuilder().comCodigo(2).construir());
        sut.adicionar(new CidadeBuilder().comCodigo(3).construir());

        Cidade removida = sut.remover(1);

        assertEquals(2, sut.cidadesNaLista());
        assertEquals(2, removida.getCodigo());
    }
}
