package br.ufg.pos.fswm.foo.exercicio1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class ComplexoTest {

    public static final double DELTA = 0.00001;
    private Complexo sut;

    @Test
    public void deve_ser_possivel_instanciar_um_numero_complexo_com_valores_padroes() throws Exception {
        sut = new Complexo();

        assertEquals(0.0, sut.getParteReal(), DELTA);
        assertEquals(0.0, sut.getParteImaginaria(), DELTA);
    }

    @Test
    public void deve_ser_possivel_instanciar_um_numero_complexo_passando_argumentos_no_construtor() throws Exception {
        sut = new Complexo(5.0, 6.0);

        assertEquals(5.0, sut.getParteReal(), DELTA);
        assertEquals(6.0, sut.getParteImaginaria(), DELTA);
    }

    @Test
    public void deve_ser_possivel_somar_um_numero_complexo_recebendo_outro_como_parametro() throws Exception {
        sut = new Complexo(5.0, 8.0);

        sut.somar(new Complexo(1.0, 2.0));

        assertEquals(6.0, sut.getParteReal(), DELTA);
        assertEquals(10.0, sut.getParteImaginaria(), DELTA);
    }

    @Test
    public void deve_ser_possivel_subtrair_um_numero_complexo_recebendo_outro_como_parametro() throws Exception {
        sut = new Complexo(5.0, 8.0);

        sut.subtrair(new Complexo(1.0, 2.0));

        assertEquals(4.0, sut.getParteReal(), DELTA);
        assertEquals(6.0, sut.getParteImaginaria(), DELTA);
    }

    @Test
    public void deve_ser_possivel_multiplicar_um_numero_complexo_recebendo_outro_como_parametro() throws Exception {
        sut = new Complexo(5.0, 8.0);

        sut.multiplicar(new Complexo(1.0, 2.0));

        assertEquals(-11.0, sut.getParteReal(), DELTA);
        assertEquals(18.0, sut.getParteImaginaria(), DELTA);
    }

    @Test
    public void deve_mostrar_os_numeros_complexos_em_formato_string() throws Exception {
        sut = new Complexo(5.0, 8.0);

        String resultado = sut.mostrar();

        assertEquals("(5.0, 8.0)", resultado);
    }
}
