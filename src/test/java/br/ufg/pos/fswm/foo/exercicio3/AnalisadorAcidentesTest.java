package br.ufg.pos.fswm.foo.exercicio3;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class AnalisadorAcidentesTest {

    private AnalisadorAcidentes sut;

    private Cidade goiania;
    private Cidade saoPaulo;
    private Cidade curitiba;
    private Cidade anapolis;
    private Cidade saoBernardo;
    private Cidade aparecida;

    @Before
    public void setUp() throws Exception {
        goiania = new CidadeBuilder().comCodigo(1).comNome("Goiania").comUF("GO").veiculos(500000).acidentes(25000).construir();
        saoPaulo = new CidadeBuilder().comCodigo(2).comNome("São Paulo").comUF("SP").veiculos(6500000).acidentes(300000).construir();
        curitiba = new CidadeBuilder().comCodigo(3).comNome("Curitiba").comUF("PR").veiculos(5000).acidentes(800).construir();
        anapolis = new CidadeBuilder().comCodigo(4).comNome("Anápolis").comUF("GO").veiculos(900000).acidentes(2530).construir();
        saoBernardo = new CidadeBuilder().comCodigo(5).comNome("São Bernardo do Campo").comUF("SP").veiculos(100000).acidentes(84560).construir();
        aparecida = new CidadeBuilder().comCodigo(6).comNome("Aparecida de Goiânia").comUF("GO").veiculos(40000).acidentes(38100).construir();

        sut = new AnalisadorAcidentes(new Cidade[] {goiania, saoPaulo, curitiba, anapolis, saoBernardo, aparecida});
    }

    @Test
    public void deve_retornar_a_cidade_com_maior_numero_de_acidentes() throws Exception {
        Cidade resultado = sut.maiorNumeroAcidentes();
        
        assertEquals(saoPaulo, resultado);
    }

    @Test
    public void deve_retornar_a_cidade_com_menor_numero_de_acidentes() throws Exception {
        Cidade resultado = sut.menorNumeroAcidentes();

        assertEquals(curitiba, resultado);
    }

    @Test
    public void deve_retornar_a_media_de_veiculos_das_cidades_brasileiras() throws Exception {
        assertEquals(1340833, sut.mediaVeiculos());
    }

    @Test
    public void deve_devolver_a_media_de_acidentes_entre_as_cidades_de_goias() throws Exception {
        assertEquals(21876, sut.mediaAcidentes("GO"));
    }
}
