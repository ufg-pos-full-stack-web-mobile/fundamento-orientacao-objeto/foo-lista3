package br.ufg.pos.fswm.foo.exercicio6;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class SexoTest {

    @Test
    public void deve_ser_possivel_inferir_sexo_masculino_atraves_do_codigo_m() throws Exception {
        Sexo sexo = Sexo.fromCodigo("m");

        assertEquals(Sexo.MASCULINO, sexo);
    }

    @Test
    public void deve_ser_possivel_inferir_sexo_masculino_atraves_do_codigo_M() throws Exception {
        Sexo sexo = Sexo.fromCodigo("M");

        assertEquals(Sexo.MASCULINO, sexo);
    }

    @Test
    public void deve_ser_possivel_inferir_sexo_feminino_atraves_do_codigo_f() throws Exception {
        Sexo sexo = Sexo.fromCodigo("f");

        assertEquals(Sexo.FEMININO, sexo);
    }

    @Test
    public void deve_ser_possivel_inferir_sexo_feminino_atraves_do_codigo_F() throws Exception {
        Sexo sexo = Sexo.fromCodigo("F");

        assertEquals(Sexo.FEMININO, sexo);
    }
}
