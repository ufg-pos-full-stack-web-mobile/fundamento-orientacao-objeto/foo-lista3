package br.ufg.pos.fswm.foo.exercicio6;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class CalculadoraOperacaoFabricaTest {

    public static final double DELTA = 0.0001;
    private CalculadoraOperacaoFabrica sut;

    @Before
    public void setUp() throws Exception {
        Operario op1 = new OperarioBuilder().matricula(1).sexo(Sexo.MASCULINO).fabricou(25).construir(); // 1750
        Operario op2 = new OperarioBuilder().matricula(2).sexo(Sexo.FEMININO).fabricou(12).construir();
        Operario op3 = new OperarioBuilder().matricula(3).sexo(Sexo.MASCULINO).fabricou(5).construir();
        Operario op4 = new OperarioBuilder().matricula(4).sexo(Sexo.MASCULINO).fabricou(9).construir();
        Operario op5 = new OperarioBuilder().matricula(5).sexo(Sexo.FEMININO).fabricou(39).construir(); // 1360
        Operario op6 = new OperarioBuilder().matricula(6).sexo(Sexo.FEMININO).fabricou(45).construir(); // 1900

        sut = new CalculadoraOperacaoFabrica(Arrays.asList(op1, op2, op3, op4, op5, op6));
    }

    @Test
    public void deve_calcular_folha_de_pagamento_dos_funcionarios() throws Exception {
        final double folha = sut.calcularFolha();

        assertEquals(8110.0, folha, DELTA);
    }

    @Test
    public void deve_calcular_total_de_pecas_fabricadas_por_mes() throws Exception {
        final int pecasFabricadas = sut.calcularTotalPecasFabricadas();

        assertEquals(135, pecasFabricadas);
    }

    @Test
    public void deve_calcular_media_pecas_fabricadas_por_homens_da_cateria_A() throws Exception {
        final int media = sut.calcularMedia(Categoria.A, Sexo.MASCULINO);

        assertEquals(7, media);
    }

    @Test
    public void deve_calcular_media_pecas_fabricadas_por_mulheres_da_categoria_C() throws Exception {
        final int media = sut.calcularMedia(Categoria.C, Sexo.FEMININO);

        assertEquals(42, media);
    }

    @Test
    public void deve_retornar_a_matricula_do_funcionario_com_maior_salario() throws Exception {
        Operario operario = sut.maiorSalario();
        assertEquals(6, operario.getMatricula());
    }
}
