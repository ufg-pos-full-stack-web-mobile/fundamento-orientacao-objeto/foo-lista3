package br.ufg.pos.fswm.foo.exercicio6;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class CategoriaTest {

    public static final double DELTA = 0.0001;

    private void validarCategoriaPorPecasFabricadas(int qtdPecasFabricadas, Categoria esperado) {
        Categoria categoria = Categoria.getCategoria(qtdPecasFabricadas);

        assertEquals(esperado, categoria);
    }

    @Test
    public void deve_retornar_categoriaA_para_quantidade_de_pecas_fabricada_menor_que_10() throws Exception {
        validarCategoriaPorPecasFabricadas(8, Categoria.A);
    }

    @Test
    public void deve_retornar_categoriaA_para_quantidade_de_pecas_fabricadas_igual_a_10() throws Exception {
        validarCategoriaPorPecasFabricadas(10, Categoria.A);
    }

    @Test
    public void deve_retornar_categoriaB_para_quantidade_de_pecas_fabricadas_maior_que_10() throws Exception {
        validarCategoriaPorPecasFabricadas(11, Categoria.B);
    }

    @Test
    public void deve_retornar_categoriaB_para_quantidade_de_pecas_fabricadas_igual_a_35() throws Exception {
        validarCategoriaPorPecasFabricadas(35, Categoria.B);
    }

    @Test
    public void deve_retornar_categoriaC_para_quantidade_de_pecas_fabricadas_maior_que_35() throws Exception {
        validarCategoriaPorPecasFabricadas(36, Categoria.C);
    }

    @Test
    public void deve_retornar_0_para_percentual_de_aumento_para_categoriaA() throws Exception {
        final Categoria categoria = Categoria.A;
        assertEquals(0.0, categoria.getPercentualAumento(), DELTA);
    }

    @Test
    public void deve_retornar_5_porcento_de_percentual_de_aumento_para_categoriaB() throws Exception {
        final Categoria categoria = Categoria.B;
        assertEquals(0.05, categoria.getPercentualAumento(), DELTA);
    }

    @Test
    public void deve_retornar_9_porcento_de_percentual_de_aumento_para_categoriaC() throws Exception {
        final Categoria categoria = Categoria.C;
        assertEquals(0.09, categoria.getPercentualAumento(), DELTA);
    }
}
