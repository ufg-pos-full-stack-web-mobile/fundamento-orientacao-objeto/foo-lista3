package br.ufg.pos.fswm.foo.exercicio6;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class CalculadoraSalarioImplTest {

    public static final double DELTA = 0.0001;
    private CalculadoraSalario sut;

    @Before
    public void setUp() throws Exception {
        sut = new CalculadoraSalarioImpl();
    }

    private void verificarSalario(double salarioEsperado, int qtdPecasFabricadas) {
        final double salario = sut.calcularSalario(qtdPecasFabricadas);

        assertEquals(salarioEsperado, salario, DELTA);
    }

    @Test
    public void deve_calcular_salario_de_funcionario_da_categoria_A() throws Exception {
        verificarSalario(SalarioMinimo.VALOR, 8);
    }

    @Test
    public void deve_calcular_salario_de_funcionario_da_categoria_A_que_fabricou_o_maximo_de_pecas() throws Exception {
        verificarSalario(SalarioMinimo.VALOR, 10);
    }

    @Test
    public void deve_calcular_salario_de_funcionario_da_categoria_B_que_fabricou_o_minimo_de_pecas() throws Exception {
        verificarSalario(1050.0, 11);
    }

    @Test
    public void deve_calcular_salario_de_funcionario_da_categoria_B_que_fabricou_o_maximo_de_pecas() throws Exception {
        verificarSalario(2250.0, 35);
    }

    @Test
    public void deve_calcular_salario_de_funcionario_da_categoria_C_que_fabricou_o_minimo_de_pecas() throws Exception {
        verificarSalario(1090.0, 36);
    }

    @Test
    public void deve_calcular_salario_de_funcionario_da_categoria_C_que_fabricou_mais_pecas() throws Exception {
        verificarSalario(2350.0, 50);
    }
}
