package br.ufg.pos.fswm.foo.exercicio3;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class CidadeBuilder {

    private Cidade instance;

    public CidadeBuilder() {
        instance = new Cidade();
    }

    public CidadeBuilder comCodigo(int codigo) {
        instance.setCodigo(codigo);
        return this;
    }

    public CidadeBuilder comNome(String nome) {
        instance.setNome(nome);
        return this;
    }

    public CidadeBuilder comUF(String uf) {
        instance.setEstado(uf);
        return this;
    }

    public CidadeBuilder veiculos(int qtdVeiculos) {
        instance.setQuantidadeVeiculosPasseio(qtdVeiculos);
        return this;
    }

    public CidadeBuilder acidentes(int qtdAcidentes) {
        instance.setQuantidadeAcidentesTransito(qtdAcidentes);
        return this;
    }

    public Cidade construir() {
        return instance;
    }
}
