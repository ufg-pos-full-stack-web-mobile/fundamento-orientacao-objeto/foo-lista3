package br.ufg.pos.fswm.foo.exercicio5;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class Funcionario {

    private double salarioFixo;
    private int numeroInscricao;
    private int qtdTvTelaPlanaVendida;
    private int qtdTvTelaComumVendida;

    public double calcularSalarioBruto() {
        return salarioFixo + calcularComissao();
    }

    public double calcularComissao() {
        double comissao = 0.0;

        if(qtdTvTelaPlanaVendida < 10) {
            comissao += qtdTvTelaPlanaVendida * 30.0;
        } else {
            comissao += qtdTvTelaPlanaVendida * 50.0;
        }

        if(qtdTvTelaComumVendida < 20) {
            comissao += qtdTvTelaComumVendida * 20.0;
        } else {
            comissao += qtdTvTelaComumVendida * 30.0;
        }

        return comissao;
    }

    public double calcularINSS() {
        return (salarioFixo + calcularComissao()) * 0.08;
    }

    public double calcularIR() {
        final double salarioTotal = (salarioFixo + calcularComissao()) - calcularINSS();
        if(salarioTotal < 700) {
            return 0.0;
        }
        return salarioTotal * 0.05;
    }

    public double calcularSalarioLiquido() {
        return calcularSalarioBruto() - calcularINSS() - calcularIR();
    }

    public void setSalarioFixo(double salarioFixo) {
        this.salarioFixo = salarioFixo;
    }

    public double getSalarioFixo() {
        return salarioFixo;
    }

    public int getNumeroInscricao() {
        return numeroInscricao;
    }

    public void setNumeroInscricao(int numeroInscricao) {
        this.numeroInscricao = numeroInscricao;
    }

    public void setQtdTvTelaPlanaVendida(int qtdTvTelaPlanaVendida) {
        this.qtdTvTelaPlanaVendida = qtdTvTelaPlanaVendida;
    }

    public int getQtdTvTelaPlanaVendida() {
        return qtdTvTelaPlanaVendida;
    }

    public void setQtdTvTelaComumVendida(int qtdTvTelaComumVendida) {
        this.qtdTvTelaComumVendida = qtdTvTelaComumVendida;
    }

    public int getQtdTvTelaComumVendida() {
        return qtdTvTelaComumVendida;
    }
}
