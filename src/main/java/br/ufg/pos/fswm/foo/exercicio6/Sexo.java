package br.ufg.pos.fswm.foo.exercicio6;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public enum Sexo {
    MASCULINO("m"), FEMININO("f");

    private String codigo;

    Sexo(String codigo) {
        this.codigo = codigo;
    }

    public static Sexo fromCodigo(String codigo) {
        for (Sexo sexo : values()) {
            if (codigo.equalsIgnoreCase(sexo.getCodigo())) {
                return sexo;
            }
        }
        return null;
    }

    public String getCodigo() {
        return codigo;
    }
}
