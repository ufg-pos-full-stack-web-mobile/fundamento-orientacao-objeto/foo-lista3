package br.ufg.pos.fswm.foo.exercicio6;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public enum Categoria {

    A(0, 10, 0),
    B(10, 35, 0.05),
    C(35, Integer.MAX_VALUE, 0.09);

    private final int quantidadeMinimaPecas;
    private final int quantidadeMaximaPecas;
    private final double percentualAumento;

    Categoria(int quantidadeMinimaPecas, int quantidadeMaximaPecas, double percentualAumento) {
        this.quantidadeMinimaPecas = quantidadeMinimaPecas;
        this.quantidadeMaximaPecas = quantidadeMaximaPecas;
        this.percentualAumento = percentualAumento;
    }

    public static Categoria getCategoria(int qtdPecasFabricadas) {
        for(Categoria categoria : values()) {
            if(qtdPecasFabricadas > categoria.getQuantidadeMinimaPecas() &&
                    qtdPecasFabricadas <= categoria.getQuantidadeMaximaPecas()) {
                return categoria;
            }
        }
        return null;
    }

    public int getQuantidadeMinimaPecas() {
        return quantidadeMinimaPecas;
    }

    public int getQuantidadeMaximaPecas() {
        return quantidadeMaximaPecas;
    }

    public double getPercentualAumento() {
        return percentualAumento;
    }
}
