package br.ufg.pos.fswm.foo.exercicio6;

import br.ufg.pos.fswm.foo.exercicio3.CidadeBuilder;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class OperarioBuilder {

    private Operario instance;

    public OperarioBuilder() {
        instance = new Operario();
    }

    public OperarioBuilder matricula(int matricula) {
        instance.setMatricula(matricula);
        return this;
    }

    public OperarioBuilder sexo(Sexo sexo) {
        instance.setSexo(sexo);
        return this;
    }

    public OperarioBuilder fabricou(int pecas) {
        instance.setPecasFabricadas(pecas);
        return this;
    }

    public Operario construir() {
        return instance;
    }
}
