package br.ufg.pos.fswm.foo.exercicio3;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class AnalisadorAcidentes {
    private final Cidade[] cidades;

    public AnalisadorAcidentes(Cidade[] cidades) {
        this.cidades = cidades;
    }

    public Cidade maiorNumeroAcidentes() {
        Cidade maisAcidentes = new CidadeBuilder().acidentes(Integer.MIN_VALUE).construir();

        for(Cidade cidade : cidades) {
            if(cidade.getQuantidadeAcidentesTransito() >  maisAcidentes.getQuantidadeAcidentesTransito()) {
                maisAcidentes = cidade;
            }
        }

        return maisAcidentes;
    }

    public Cidade menorNumeroAcidentes() {
        Cidade menosAcidentes = new CidadeBuilder().acidentes(Integer.MAX_VALUE).construir();

        for(Cidade cidade : cidades) {
            if(cidade.getQuantidadeAcidentesTransito() < menosAcidentes.getQuantidadeAcidentesTransito()) {
                menosAcidentes = cidade;
            }
        }

        return menosAcidentes;
    }

    public int mediaVeiculos() {
        int totalVeiculos = 0;

        for(Cidade cidade : cidades) {
            totalVeiculos += cidade.getQuantidadeVeiculosPasseio();
        }

        return totalVeiculos / cidades.length;
    }

    public int mediaAcidentes(String uf) {
        int totalAcidentes = 0;
        int quantidadeCidade = 0;

        for(Cidade cidade : cidades) {
            if(cidade.getEstado().equals(uf)) {
                totalAcidentes += cidade.getQuantidadeAcidentesTransito();
                quantidadeCidade++;
            }
        }

        return totalAcidentes / quantidadeCidade;
    }
}
