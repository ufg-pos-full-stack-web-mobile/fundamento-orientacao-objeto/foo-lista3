package br.ufg.pos.fswm.foo.exercicio7;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class Prova {
    private int numInscricao;
    private String nome;
    private String telefone;
    private Map<Integer, Integer> respostas = new HashMap<Integer, Integer>(10);
    private Gabarito gabaritoRespostas;

    public void responder(int numQuestao, int resposta) {
        this.respostas.put(numQuestao, resposta);
    }

    public int getResposta(int numQuestao) {
        return this.respostas.get(numQuestao);
    }

    public int calcularNota() {
        int nota = 0;

        for(Map.Entry<Integer, Integer> resposta : this.respostas.entrySet()) {
            if(this.gabaritoRespostas.getRespostaDaQuestao(resposta.getKey()) == resposta.getValue()) {
                nota++;
            }
        }

        return nota;
    }

    public int getNumInscricao() {
        return numInscricao;
    }

    public void setNumInscricao(int numInscricao) {
        this.numInscricao = numInscricao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setGabaritoRespostas(Gabarito gabaritoRespostas) {
        this.gabaritoRespostas = gabaritoRespostas;
    }

    public String monstrarDados() {
        return String.format("Dados Candidato:\nInscrição: %d\nNome: %s\nNota: %d",
                this.numInscricao, this.nome, this.calcularNota());
    }
}
