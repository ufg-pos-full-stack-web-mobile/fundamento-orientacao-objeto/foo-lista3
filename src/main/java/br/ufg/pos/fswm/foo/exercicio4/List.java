package br.ufg.pos.fswm.foo.exercicio4;

import br.ufg.pos.fswm.foo.exercicio3.Cidade;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class List {

    private final int capacidadeList;
    private Cidade[] cidades;
    private int posicaoAtual;

    public List(int tamanho) {
        this.capacidadeList = tamanho;
        cidades = new Cidade[capacidadeList];
        posicaoAtual = 0;
    }

    public int getCapacidadeList() {
        return capacidadeList;
    }

    public void adicionar(Cidade cidade) {
        cidades[posicaoAtual++] = cidade;
    }

    public int cidadesNaLista() {
        return posicaoAtual;
    }

    public Cidade remover(int indice) {
        Cidade removida = cidades[indice];
        cidades[indice] = null;

        for(int i = indice; i < capacidadeList-1 ; i++) {
            Cidade temp = cidades[i];
            cidades[i] = cidades[i+1];
            cidades[i+1] = temp;
        }

        posicaoAtual--;

        return removida;
    }
}
