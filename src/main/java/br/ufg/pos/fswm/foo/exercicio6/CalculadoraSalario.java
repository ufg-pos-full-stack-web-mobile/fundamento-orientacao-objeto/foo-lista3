package br.ufg.pos.fswm.foo.exercicio6;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public interface CalculadoraSalario {

    double calcularSalario(int qtdPecasFabricadas);

}
