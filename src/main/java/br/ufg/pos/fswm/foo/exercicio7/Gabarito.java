package br.ufg.pos.fswm.foo.exercicio7;

import java.util.Map;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class Gabarito {

    private final Map<Integer, Integer> respostas;

    public Gabarito(Map<Integer, Integer> respostas) {
        this.respostas = respostas;
    }

    public Integer getRespostaDaQuestao(Integer numQuestao) {
        return respostas.get(numQuestao);
    }

}
