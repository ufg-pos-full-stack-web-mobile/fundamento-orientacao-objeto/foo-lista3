package br.ufg.pos.fswm.foo.exercicio6;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class CalculadoraSalarioImpl implements CalculadoraSalario {

    public double calcularSalario(int qtdPecasFabricadas) {
        final Categoria categoria = Categoria.getCategoria(qtdPecasFabricadas);

        return SalarioMinimo.VALOR + (SalarioMinimo.VALOR * categoria.getPercentualAumento() * (qtdPecasFabricadas - categoria.getQuantidadeMinimaPecas()));
    }

}
