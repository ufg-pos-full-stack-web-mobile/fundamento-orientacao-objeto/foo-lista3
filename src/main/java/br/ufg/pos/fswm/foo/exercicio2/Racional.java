package br.ufg.pos.fswm.foo.exercicio2;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class Racional {
    private int numerador;
    private int denominador;

    public Racional() {
        this.numerador = 0;
        this.denominador = 0;
    }

    public Racional(int numerador, int denominador) {
        reduzirFracao(numerador, denominador);
    }

    public void somar(Racional racional) {
        int mmc = this.denominador * racional.getDenominador();

        int parte1 = mmc / this.denominador * this.numerador;
        int parte2 = mmc / racional.getDenominador() * racional.getNumerador();

        reduzirFracao(parte1 + parte2, mmc);
    }

    public void subtrair(Racional racional) {
        int mmc = this.denominador * racional.getDenominador();

        int parte1 = mmc / this.denominador * this.numerador;
        int parte2 = mmc / racional.getDenominador() * racional.getNumerador();

        reduzirFracao(parte1 - parte2, mmc);
    }

    public void multiplicar(Racional racional) {
        int numerador = this.numerador * racional.getNumerador();
        int denominador = this.denominador * racional.getDenominador();

        reduzirFracao(numerador, denominador);
    }

    public void dividir(Racional racional) {
        int numerador = this.numerador * racional.getDenominador();
        int denominador = this.denominador * racional.getNumerador();

        reduzirFracao(numerador, denominador);
    }

    public String mostrar() {
        return String.format("%d/%d", this.numerador, this.denominador);
    }

    private void reduzirFracao(int numerador, int denominador) {
        int mdc = calcularMDC(numerador, denominador);

        this.numerador = numerador / mdc;
        this.denominador = denominador / mdc;
    }

    private int calcularMDC(int numerador, int denominador) {
        if(denominador == 0) {
            return numerador;
        }
        return calcularMDC(denominador, numerador % denominador);
    }

    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public int getNumerador() {
        return numerador;
    }

    public void setDenominador(int denominador) {
        this.denominador = denominador;
    }

    public int getDenominador() {
        return denominador;
    }
}
