package br.ufg.pos.fswm.foo.exercicio5;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class Main {

    public static final int QTD_FUNCIONARIOS = 35;

    public static void main(String... args) {
        List<Funcionario> funcionarios = new ArrayList<Funcionario>();

        for(int i = 0; i < QTD_FUNCIONARIOS; i++) {
            final Funcionario f = new Funcionario();
            f.setNumeroInscricao(capturarInt("número inscrição do funcionario " + (i+1)));
            f.setSalarioFixo(capturarDouble("salário fixo mensal do funcionário " + (i+1)));
            f.setQtdTvTelaPlanaVendida(capturarInt("quantidade de TVs de tela plana vendida pelo funcionário " + i+1));
            f.setQtdTvTelaComumVendida(capturarInt("quantidade de TVs de tela comum vendida pelo funcionário " + i+1));
            funcionarios.add(f);
        }

        for(int i = 0; i < QTD_FUNCIONARIOS; i++) {
            final String mensagem = String.format("Dados do funcionário %d\nNúmero de inscrição: %d\nSalário Bruto: %.2f\nSalário Líquido: %.2f",
                    i+1, funcionarios.get(i).getNumeroInscricao(), funcionarios.get(i).calcularSalarioBruto(), funcionarios.get(i).calcularSalarioLiquido());
            JOptionPane.showMessageDialog(null, mensagem);
        }
    }

    private static double capturarDouble(String campo) {
        boolean isInvalido = true;
        double valor = 0.0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                valor = Double.parseDouble(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a) " + campo + " válido(a)!");
            }
        } while(isInvalido);

        return valor;
    }

    private static int capturarInt(String campo) {
        boolean isInvalido = true;
        int codigoAumento = 0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                codigoAumento = Integer.parseInt(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a) " + campo + " válido(a)!");
            }
        } while(isInvalido);

        return codigoAumento;
    }
}
