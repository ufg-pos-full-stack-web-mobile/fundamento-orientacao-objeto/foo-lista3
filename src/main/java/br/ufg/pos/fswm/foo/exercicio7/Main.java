package br.ufg.pos.fswm.foo.exercicio7;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class Main {

    public static void main(String... args) {

        List<Prova> provas = new ArrayList<Prova>();

        capturarCandidatos(provas);

        Gabarito gabarito = montarGabaritoRespostas();

        informarGabaritoNasProvas(provas, gabarito);

        preencherRespostasDosAlunos(provas);

        informarPercentualDeAlunosAprovados(provas);

        mostrarListaDosAprovados(provas);

        informarPercentualDeProvasComNotaMaiorQueNove(provas);

    }

    private static void informarPercentualDeProvasComNotaMaiorQueNove(List<Prova> provas) {
        int qtdAlunosAprovados = 0;
        for(Prova prova : provas) {
            if(prova.calcularNota() >= 9) {
                qtdAlunosAprovados++;
            }
        }
        final String informePercentualAlunosAprovados = String.format("%.1f %% das provas tiveram notas acima de 9", ((double) qtdAlunosAprovados/provas.size()));
    }

    private static void mostrarListaDosAprovados(List<Prova> provas) {
        StringBuilder sb = new StringBuilder();
        sb.append("APROVADOS");
        for(Prova prova : provas) {
            if(prova.calcularNota() >= 6) {
                sb.append(String.format("Número Inscrição: %d\nNome: %s\nTelefone: %s\n\n",
                        prova.getNumInscricao(), prova.getNome(), prova.getTelefone()));
            }
        }
        JOptionPane.showMessageDialog(null, sb.toString());
    }

    private static void informarPercentualDeAlunosAprovados(List<Prova> provas) {
        int qtdAlunosAprovados = 0;
        for(Prova prova : provas) {
            if(prova.calcularNota() >= 6) {
                qtdAlunosAprovados++;
            }
        }
        final String informePercentualAlunosAprovados = String.format("Foram aprovados %.1f %% dos alunos", ((double) qtdAlunosAprovados/provas.size()));
    }

    private static void preencherRespostasDosAlunos(List<Prova> provas) {
        for(Prova prova : provas) {
            for(int i = 1 ; i <= 10 ; i++) {
                int resposta = capturarInt("resposta da questão " + i + " do candidato " + prova.getNumInscricao());
                prova.responder(i, resposta);
            }
        }
    }

    private static void informarGabaritoNasProvas(List<Prova> provas, Gabarito gabarito) {
        for(Prova prova : provas) {
            prova.setGabaritoRespostas(gabarito);
        }
    }

    private static Gabarito montarGabaritoRespostas() {
        Map<Integer, Integer> gabaritoResp = new HashMap<Integer, Integer>(10);
        for(int i = 1; i <= 10 ; i++) {
            final int resposta = capturarInt("resposta da questão " + i);
            gabaritoResp.put(i, resposta);
        }
        return new Gabarito(gabaritoResp);
    }

    private static void capturarCandidatos(List<Prova> provas) {
        boolean continuar = true;

        do {
            int numInscricao = capturarInt("número de inscricao do candidato (Digite 0 para sair)");
            if(numInscricao == 0) {
                continuar = false;
            } else {
                String nome = JOptionPane.showInputDialog("Digite o nome do candidato");
                String telefone = JOptionPane.showInputDialog("Digite o telefone do candidato");

                Prova prova = new Prova();
                prova.setNumInscricao(numInscricao);
                prova.setNome(nome);
                prova.setTelefone(telefone);

                provas.add(prova);
            }
        } while(continuar);
    }


    private static int capturarInt(String campo) {
        boolean isInvalido = true;
        int resposta = 0;
        do {
            final String valorStr = JOptionPane.showInputDialog("Digite o(a) " + campo);
            try {
                resposta = Integer.parseInt(valorStr);
                isInvalido = false;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "Preencha um(a) " + campo + " válido(a)!");
            }
        } while(isInvalido);

        return resposta;
    }
}
