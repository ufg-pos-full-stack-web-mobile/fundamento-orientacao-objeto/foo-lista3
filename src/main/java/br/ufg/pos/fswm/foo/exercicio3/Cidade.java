package br.ufg.pos.fswm.foo.exercicio3;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class Cidade {

    private int codigo;
    private String nome;
    private String estado;
    private int quantidadeVeiculosPasseio;
    private int quantidadeAcidentesTransito;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getQuantidadeVeiculosPasseio() {
        return quantidadeVeiculosPasseio;
    }

    public void setQuantidadeVeiculosPasseio(int quantidadeVeiculosPasseio) {
        this.quantidadeVeiculosPasseio = quantidadeVeiculosPasseio;
    }

    public int getQuantidadeAcidentesTransito() {
        return quantidadeAcidentesTransito;
    }

    public void setQuantidadeAcidentesTransito(int quantidadeAcidentesTransito) {
        this.quantidadeAcidentesTransito = quantidadeAcidentesTransito;
    }
}
