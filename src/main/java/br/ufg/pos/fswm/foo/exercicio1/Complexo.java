package br.ufg.pos.fswm.foo.exercicio1;

import java.util.Locale;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 21/04/17.
 */
public class Complexo {
    private double parteReal;
    private double parteImaginaria;

    public Complexo() {
        this.parteReal = 0.0;
        this.parteImaginaria = 0.0;
    }

    public Complexo(double parteReal, double parteImaginaria) {
        this.parteReal = parteReal;
        this.parteImaginaria = parteImaginaria;
    }

    public void somar(Complexo complexo) {
        this.parteReal += complexo.getParteReal();
        this.parteImaginaria += complexo.getParteImaginaria();
    }

    public void subtrair(Complexo complexo) {
        this.parteReal -= complexo.getParteReal();
        this.parteImaginaria -= complexo.getParteImaginaria();
    }

    public void multiplicar(Complexo complexo) {
        double novaParteReal = calculaParteRealMultiplicacao(complexo);
        double novaParteImaginaria = calculaParteImaginariaMultiplicacao(complexo);

        this.parteReal = novaParteReal;
        this.parteImaginaria = novaParteImaginaria;
    }

    public String mostrar() {
        return String.format(Locale.US, "(%.1f, %.1f)", this.parteReal, this.parteImaginaria);
    }

    private double calculaParteImaginariaMultiplicacao(Complexo complexo) {
        return (this.parteReal * complexo.getParteImaginaria()) + (this.parteImaginaria * complexo.getParteReal());
    }

    private double calculaParteRealMultiplicacao(Complexo complexo) {
        return (this.parteReal * complexo.getParteReal()) - (this.parteImaginaria * complexo.getParteImaginaria());
    }

    public double getParteReal() {
        return parteReal;
    }

    public double getParteImaginaria() {
        return parteImaginaria;
    }

}
