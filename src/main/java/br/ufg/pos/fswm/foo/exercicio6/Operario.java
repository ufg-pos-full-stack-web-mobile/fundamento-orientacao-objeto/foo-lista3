package br.ufg.pos.fswm.foo.exercicio6;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class Operario {
    private int matricula;
    private int pecasFabricadas;
    private Sexo sexo;

    public double calcularSalario() {
        return new CalculadoraSalarioImpl().calcularSalario(pecasFabricadas);
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public int getPecasFabricadas() {
        return pecasFabricadas;
    }

    public void setPecasFabricadas(int pecasFabricadas) {
        this.pecasFabricadas = pecasFabricadas;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Categoria getCategoria() {
        return Categoria.getCategoria(pecasFabricadas);
    }
}
