package br.ufg.pos.fswm.foo.exercicio6;

import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 22/04/17.
 */
public class CalculadoraOperacaoFabrica {
    private final List<Operario> operarios;

    public CalculadoraOperacaoFabrica(List<Operario> operarios) {
        this.operarios = operarios;
    }

    public double calcularFolha() {
        double folha = 0.0;

        for(Operario operario : operarios) {
            folha += operario.calcularSalario();
        }

        return folha;
    }

    public int calcularTotalPecasFabricadas() {
        int pecasFabricadas = 0;
        for(Operario operario : operarios) {
            pecasFabricadas += operario.getPecasFabricadas();
        }
        return pecasFabricadas;
    }

    public int calcularMedia(Categoria categoria, Sexo sexo) {
        int totalPecas = 0;
        int qtdFuncionarios = 0;

        for(Operario operario : operarios) {
            if(operario.getCategoria().equals(categoria) && operario.getSexo().equals(sexo)) {
                totalPecas += operario.getPecasFabricadas();
                qtdFuncionarios++;
            }
        }

        return totalPecas / qtdFuncionarios;
    }

    public Operario maiorSalario() {
        double maior = Double.MIN_VALUE;
        Operario operarioMaior = null;

        for(Operario operario : operarios) {
            final double salarioOperario = operario.calcularSalario();
            if(salarioOperario > maior) {
                maior = salarioOperario;
                operarioMaior = operario;
            }
        }

        return operarioMaior;
    }
}
